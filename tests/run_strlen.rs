use b7::b7tui::Env;
use b7::dynamorio;
use b7::B7Opts;
use std::collections::HashMap;
use std::path::PathBuf;

#[test]
fn run_strlenx86test() {
    let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    let mut dynpath = path.clone();

    path.push("tests");
    path.push("strlen_test_x86");

    dynpath.push("dynamorio");
    dynpath.push("build");

    let mut term = Env::new();
    let mut vars = HashMap::new();
    vars.insert(
        "dynpath".to_string(),
        dynpath.to_string_lossy().into_owned(),
    );

    let mut opts = B7Opts::new(
        path.to_string_lossy().into_owned(),
        true,
        false,
        dynamorio::get_inst_count,
        &mut term,
        vars,
    );

    let res = opts.run();
    let mut stdin = res.arg_brute.unwrap();

    // Last character is currently non-deterministic...need to include linefeed to check
    ///stdin.pop(); to deal with ^^
    println!("{:?}",stdin);
    assert_eq!(&stdin, "strlen_pass"); // change to actual strlen pwd to pass assert
}
